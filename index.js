//express
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var cors = require('cors');
app.use(express.static('./'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.set('trust proxy', 1);
app.enable('trust proxy');
app.use(function (req ,res, next) {
  if(req.secure)
    next();
  else
    res.redirect('https://' + req.headers.host + req.url);
});

// -------database settings--------------
var mongoose = require('mongoose');
mongoose.connect('mongodb://lampros18:admin@webdevelopment-shard-00-00-x6qac.mongodb.net:27017,webdevelopment-shard-00-01-x6qac.mongodb.net:27017,webdevelopment-shard-00-02-x6qac.mongodb.net:27017/books?ssl=true&replicaSet=WebDevelopment-shard-0&authSource=admin&retryWrites=true', { useNewUrlParser: true });
var booksSchema = new mongoose.Schema({
    author: String,
    title: String,
    genre: String,
    price: String
});
var Books = mongoose.model("Book", booksSchema);
// --------------------------------------
//----------url for the home page-----------------
app.get('/', function (req, res) {
    //res.sendFile('/home/it21622/assignment/frontend.html');
	res.sendFile('/app/frontend.html');
});

// HTTP Verb : POST ||url to add the books to the database
app.post('/books/', function (req, res) { //Prepared statemtns are used so as to prevent basic sql injection attacks

	    var validation = /^\d+(?:[.]\d+){0,1}$/gm;
        validation.lastIndex = 0;
	if( /^ *$/.test(req.body.author) || req.body.author.length > 25){
		        res.send(JSON.stringify({
					result : "Failure",
					reason : "The data provided have invalid values",
					server_status_code: "400"
				}));
	} else if ( /^ *$/.test(req.body.title) || req.body.title.length > 40 )
	{
			    res.send(JSON.stringify({
					result : "Failure",
					reason : "The data provided have invalid values",
					server_status_code: 400
				}));
	} else if ( req.body.genre != "Science fiction" && req.body.genre !="Satire" && req.body.genre != "Drama" && req.body.genre != "Action and Adventure" && req.body.genre != "Romance" && req.body.genre != "Mystery"
    && req.body.genre != "Horror" ){
				res.send(JSON.stringify({
					result : "Failure",
					reason : "The data provided have invalid values",
					server_status_code: 400
				}));
	}  else if ( validation.exec(req.body.price) == null ){
				res.send(JSON.stringify({
					result : "Failure",
					reason : "The data provided have invalid values",
					server_status_code: 400
				}));
	} else {
			var newBook = {author:req.body.author, title: req.body.title, genre: req.body.genre, price: req.body.price};
            Books.create(newBook, function(err, bookAdded){
                if(err){
                    res.send(JSON.stringify({
                        result : "Failure",
                        reason : "The data provided have not been inserted",
                        server_status_code: 501
                    }));
                }else{
                    let price = parseFloat(bookAdded.price);
                    res.send(JSON.stringify({
                        result : "Success",
                        author : bookAdded.author,
                        title : bookAdded.title,
                        genre : bookAdded.genre,
                        price : price
                    }));
                }
            });
    }

});

//---------HTTP verb:GET ||routes for the search function------

app.get('/books/', function (req, res) {
    Books.find( {}, function (error, books) {
            if (error) {
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({
                    id : -1,
                    result : "No match found",
                    keyword : " "
                }));
            } else {
                    res.setHeader('Content-Type', 'application/json');
                    res.send(books);
            }
    });
});



    app.get('/books/:keyword', function (req, res) {
        var keyword = req.params.keyword;
        Books.find()
        .or([ {author:new RegExp(keyword,'i')}, {title:new RegExp(keyword,'i')}, {genre:new RegExp(keyword,'i')}, {price:new RegExp(keyword)} ]).
        then(books => {res.setHeader('Content-Type', 'application/json');res.send(books)})
        .catch(error=> { res.setHeader('Content-Type', 'application/json');res.send(JSON.stringify({id : -1,result : "No match found",keyword :req.params.keyword })); });
    });

    //The wildcard * will send back the appropriate message for all the non supported urls
    app.get('*', function (req, res) {
        res.send('<p>Invalid ulr, <a href="/"> Home Page </a></p>');
    });


    app.listen(process.env.PORT, process.env.IP,function(){
        console.log("Server is running at port" + process.env.PORT);
    });
